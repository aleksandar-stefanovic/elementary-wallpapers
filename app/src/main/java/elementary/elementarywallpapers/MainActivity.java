package elementary.elementarywallpapers;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    final Context context = this;
    MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Toolbar spinner, for switching between versions
        AppCompatSpinner spinner = (AppCompatSpinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getSupportActionBar().getThemedContext(),
                R.array.planets_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position){
                    case 0: loadFreyaImageItems(myAdapter); break; //0 is Freya
                    case 1:  //TODO implement other versions
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        myAdapter = new MyAdapter(new MyAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(String item) {
                Intent intent = new Intent(context, CropActivity.class);
                intent.putExtra("image-path", item);
                startActivity(intent);
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void loadFreyaImageItems(final MyAdapter adapter) {

        List<String> items = new ArrayList<>();

        adapter.setThumbSize(imageItemSize());
        final AssetManager assetManager = context.getAssets();
        String[] filenames;
        try{
            filenames = assetManager.list(""); //empty string lists all files
            for (String path : filenames){
                Log.d("DEBUG-PATH", path);
                if (path.endsWith(".jpg") || path.endsWith(".jpeg") || path.endsWith(".png")) {
                    items.add(path);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            adapter.items = items;
            adapter.notifyDataSetChanged();
        }
    }

    private Point imageItemSize(){

        Point result = new Point();
        Point screenSize = new Point();

        Display display = getWindowManager().getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= 13) {
            display.getSize(screenSize);
        } else {
            screenSize.x = display.getWidth();
            screenSize.y = display.getHeight();
        }

        result.x = screenSize.x;
        result.y = Math.round(screenSize.x * 9 / 16);


        System.out.println(result);
        return result;
    }
}
