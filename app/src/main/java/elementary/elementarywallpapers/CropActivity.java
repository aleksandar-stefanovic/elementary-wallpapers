package elementary.elementarywallpapers;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.IOException;
import java.io.InputStream;

public class CropActivity extends AppCompatActivity {

    CropImageView cropImageView;
    Context context = this;
    private Point screenSize = new Point();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);

        Display display = getWindowManager().getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= 13) {
            display.getSize(screenSize);
        } else {
            screenSize.x = display.getWidth();
            screenSize.y = display.getHeight();
        }

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        try {
            String path = getIntent().getStringExtra("image-path");
            InputStream is = getAssets().open(path);
            Drawable drawable = Drawable.createFromStream(is, null);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            assert cropImageView != null;
            cropImageView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //this is for the "apply wallpaper" button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.crop_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_done: {

                Toast.makeText(this, "Setting wallpaper...", Toast.LENGTH_SHORT).show();

                final Bitmap bitmap = cropImageView.getCroppedImage();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        WallpaperManager wm = WallpaperManager.getInstance(context);
                        try {
                            wm.setBitmap(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view){
        switch (view.getId()){

            case R.id.action_standard:
                cropImageView.setAspectRatio(2, 1);
                cropImageView.setFixedAspectRatio(true);
            break;

            case R.id.action_static:
                cropImageView.setAspectRatio(screenSize.x, screenSize.y);
                cropImageView.setFixedAspectRatio(true);
            break;

            case R.id.action_custom:
                cropImageView.setFixedAspectRatio(false);
            break;
        }
    }
}
