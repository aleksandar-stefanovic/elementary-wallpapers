package elementary.elementarywallpapers;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ImageViewHolder> {

    List<String> items;
    OnItemClickListener listener;
    Point thumbSize;

    MyAdapter(OnItemClickListener listener){

        this.items = new ArrayList<>();
        this.listener = listener;
    }


    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {

        ViewGroup.LayoutParams params = holder.imageView.getLayoutParams();

        params.width = thumbSize.x;

        params.height = thumbSize.y;

        holder.imageView.setLayoutParams(params);

        String item = items.get(position);

        /*we need to reduce the thumbnail size to keep images in memory, and as it stands currently,
         this is a hacky workaround, when it comes to code aesthetics, but it makes the app work so
         much better. In the future, if we manage to improve performance, we can increase the size
         back to 100%
         */
        Double x = thumbSize.x * 0.7;
        Double y = thumbSize.y * 0.7;

        holder.bind(items.get(position), listener);

        Picasso.with(holder.imageView.getContext())
                .load("file:///android_asset/" + item)
                .resize(x.intValue(), y.intValue())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public interface OnItemClickListener {
        void onItemClick(String item);
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public ImageViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.image);

        }

        public void bind(final String item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public void setThumbSize(Point thumbSize) {
        this.thumbSize = thumbSize;
    }
}
